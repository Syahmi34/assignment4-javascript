/*Name: MUHAMMAD NUR SYAHMI DANIAL BIN MOHD NORHISAM
  Date: 29/6/2021*/
function submitAnswers(){
  var score = 0;
  var name = document.forms["quizForm"]["yourName"].value;
  var q1 = document.forms["quizForm"]["q1"].value;
  var q2 = document.forms["quizForm"]["q2"].value;
  var q3 = document.forms["quizForm"]["q3"].value;
  var q4 = document.forms["quizForm"]["q4"].value;
  var q5 = document.forms["quizForm"]["q5"].value;
  var q6 = document.forms["quizForm"]["q6"].value;
  var q7 = document.forms["quizForm"]["q7"].value;
  var q8 = document.forms["quizForm"]["q8"].value;
  var q9 = document.forms["quizForm"]["q9"].value;
  var q10 = document.forms["quizForm"]["q10"].value;

  if (q1 == "") {
    alert("Oops! Question 1 is required");
    return false;
  }
  if (q2 == "") {
     alert("Oops! Question 2 is required");
     return false;
  }
  if (q3 == "") {
    alert("Oops! Question 3 is required");
    return false;
  }
  if (q4 == "") {
    alert("Oops! Question 4 is required");
    return false;
  }
  if (q5 == "") {
    alert("Oops! Question 5 is required");
    return false;
  }
  if (q6 == "") {
    alert("Oops! Question 6 is required");
    return false;
  }
  if (q7 == "") {
    alert("Oops! Question 7 is required");
    return false;
  }
  if (q8 == "") {
    alert("Oops! Question 8 is required");
    return false;
  }
  if (q9 == "") {
    alert("Oops! Question 9 is required");
    return false;

  }
  if (q10 == "") {
    alert("Oops! Question 10 is required");
    return false;
  }


if (document.getElementById('q1d').checked) {
      score++;
    }
    if (document.getElementById('q2b').checked) {
        score++;
    }
    if (document.getElementById('q3c').checked) {
      score++;
    }
    if (document.getElementById('q4a').checked) {
      score++;
    }
    if (document.getElementById('q5c').checked) {
      score++;
    }
    if (document.getElementById('q6d').checked) {
      score++;
    }
    if (document.getElementById('q7a').checked) {
      score++;
    }
    if (document.getElementById('q8a').checked) {
      score++;
    }
    if (document.getElementById('q9b').checked) {
      score++;
    }
    if (document.getElementById('q10b').checked) {
      score++;
    }

    if(score>=0 && score<=4){
        alert ("Keep trying, "+name+ "! You answered " +score+ " out of 10 correctly")
    }
    else if(score>=5 && score<=9){
        alert ("Way to go, "+name+ "! You got " +score+ " out of 10 correct")
    }
    else{
        alert ("Congratulations "+name+ "! You got " +score+ " out of 10 correct")
    }
  }